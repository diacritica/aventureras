---
title: Aerendyl duerme junto a su oso
date: 2020-02-20T07:00:00
lastmod: 2020-02-20T07:00:00
author: Guina
cover: /img/aerendyl_oso.jpg
categories:
  - Presentes Sangrientos
tags:
  - escena
  - Aerendyl
---

Aerendyl, tras dormir unas pocas horas en su habitación en el palacete de Marisha Van Emris, salió fuera de éste para acompañar a su oso el resto de la noche.

<!--more-->
