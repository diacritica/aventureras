---
title: El chamán goblin es sorprendido en su sala de alquimia tras haber huido del combate
date: 2020-03-30T08:00:00
lastmod: 2020-03-30T08:00:00
author: Guina
cover: /img/chaman_goblin.jpg
categories:
  - Presentes Sangrientos
tags:
  - escena
  - PNJ
---

Durante el combate contra el Goblem, el chamán decidió escapar en un momento dado. Lo hizo a través de un pasadizo al sur de la caverna. Cuando las aventureras siguieron ese camino, lo descubrieron ocupado manipulando redomas y hierbas en su mesa de alquimia.

<!--more-->
