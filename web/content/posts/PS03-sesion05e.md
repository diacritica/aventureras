---
title: "El legado de Drom"
date: 2020-02-20T01:00:00
lastmod: 2020-02-20T01:00:00
author: Guina
cover: /img/saqueo_drom.jpg
categories:
  - Presentes Sangrientos
tags:
  - escena
  - Guina
  - Aerendyl
---

Durante el angustioso cónclave, el grupo de aventureras se ve sorprendido por un ataque sorpresa desde el interior del bosque que flanquea el camino. Drom cae víctima de una flecha mortal. Cuando vuelve la calma, Guina y Aerendyl se reparten los objetos de valor en poder del cadáver de Drom.
<!--more-->
