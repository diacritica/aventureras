---
title: Fotografía de la mesa de juego
date: 2020-02-07T00:00:00
lastmod: 2019-02-07T00:00:00
author: DM
cover: /img/aventureras01.jpg
categories:
  - Presentes Sangrientos
tags:
  - jugadoras
---

Mesa de juego en mitad de un debate. De izquierda a derecha, Kara (María), Guina (Esther), Vhenan (Angela), Aerendyl (Marina) y Bakel (Miryam).

<!--more-->
