---
title: Vhenan lucha contra el espía
date: 2020-02-20T00:30:00
lastmod: 2020-02-20T00:30:00
author: Guina
cover: /img/vhenan_vs_espia.jpg
categories:
  - Presentes Sangrientos
tags:
  - escena
  - Vhenan
---

Durante el angustioso cónclave, el grupo de aventureras se ve sorprendido por un ataque sorpresa desde el interior del bosque que flanquea el camino. Vhenan descubre el origen del ataque y se dirige rauda a confrontarlo. Se trata de una espía visirtaní que cae derrotada.
<!--more-->
