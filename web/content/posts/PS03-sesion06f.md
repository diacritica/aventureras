---
title: Bakel se recompone y ataca al Goblem con su lanza conjurada
date: 2020-03-30T05:00:00
lastmod: 2020-03-30T05:00:00
author: Guina
cover: /img/bakel_vs_goblem.jpg
categories:
  - Presentes Sangrientos
tags:
  - Bakel
  - escena
---

Bakel conjura una lanza mágica con la que ataca sin descanso al Goblem. Finalmente, consigue acertar a atravesar el cráneo del monstruo introduciendo con fuerza la lanza por la visera del yelmo del Goblem y disipando su fuerza vital.

<!--more-->
