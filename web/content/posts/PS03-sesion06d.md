---
title: Aerendyl se enfrenta a tres goblins en la caverna del Goblem
date: 2020-03-30T03:00:00
lastmod: 2020-03-30T03:00:00
author: Guina
cover: /img/aerendyl_cueva_goblins.jpg
categories:
  - Presentes Sangrientos
tags:
  - Aerendyl
  - escena
---

Aerendyl entra de lleno en el fragor del combate y se asegura de mantener a raya a varios goblins que tratan de asistir al chamán y al Goblem por un pasaje al oeste de la caverna. Aerendyl infunde a sus flechas de una poderosa magia de espinas.

<!--more-->
