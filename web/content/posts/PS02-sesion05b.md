---
title: Sorsha, la Directora de Juego
date: 2020-02-07T01:00:00
lastmod: 2019-02-07T01:00:00
author: DM
cover: /img/sorsha_DM.jpg
categories:
  - Presentes Sangrientos
tags:
  - jugadoras
---

Mesa de juego en donde Sorsha, una de las dos gatas de Angela y Pablo, muestra su deseo de dirigir la partida en cuanto el DM se ausenta un solo minuto de detrás de la pantalla. Parece que estuviera pensando "Señor, llévame pronto..."

<!--more-->
