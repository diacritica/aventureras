---
title: Aventureas "on-line"
date: 2020-03-30T00:00:00
lastmod: 2020-03-30T00:00:00
author: Guina
cover: /img/online.jpg
categories:
  - Presentes Sangrientos
tags:
  - grupo
---

Obligadas a jugar en remoto por el confinamiento, las aventureras continúan disfrutando de aventuras vía online.
Kara (María) se excusa para ésta y futuras sesiones por dificultad para conciliar ¡Kara volverá!

<!--more-->
