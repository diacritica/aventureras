---
title: Cónclave angustioso para Drom
date: 2020-02-20T00:00:00
lastmod: 2020-02-20T00:00:00
author: Guina
cover: /img/conclave_drom.jpg
categories:
  - Presentes Sangrientos
tags:
  - escena
  - grupo
---

En el camino desde La Fonda hasta Alameda, se suceden numerosos encuentros y amenazas. Tras uno de ellos, el grupo consigue atrapar con vida al que parecer ser el cabecilla, un mediano, de una embocada de salteadores de caminos.
Las aventureras debaten en voz alta sus preferencias llevando a la desesperación al prisionero, de nombre Drom.

<!--more-->
