---
title: Marisha Van Emris recibe en su palacete élfico en La Alameda
date: 2020-02-20T03:00:00
lastmod: 2020-02-20T03:00:00
author: Guina
cover: /img/marisha_van_emris.jpg
categories:
  - Presentes Sangrientos
tags:
  - escena
  - PNJ
---

Marisha Van Emris pertenece a una familia noble poderosa pero mantenida a raya por la dinastía reinante de Vigdis II. Aprovechará cualquier oportunidad para reforzar su presencia e influencia. La presencia del embajador Chandraraj de Nirmala en el grupo de aventureras es una gran oportunidad para desplegar sus deseos de incrementar su ascendencia.

<!--more-->
