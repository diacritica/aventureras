---
title: Kara invoca a un espíritu en forma de halcón
date: 2020-02-20T06:00:00
lastmod: 2020-02-20T06:00:00
author: Guina
cover: /img/kara_invoca_pajaro.jpg
categories:
  - Presentes Sangrientos
tags:
  - escena
  - Kara
---

Antes de acostarse en su habitación en el palacete de Marisha Van Emris, Kara reúne con esfuerzo y por medio de un ritual agotador, la vis mágica suficiente para invocar a un espíritu aliado en forma de halcón.

<!--more-->
