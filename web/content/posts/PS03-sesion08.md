---
title: Aventureras en camino
date: 2020-05-14T00:00:00
lastmod: 2020-05-14T00:00:00
author: Guina
cover: /img/aventureras.gif
categories:
  - Presentes Sangrientos
tags:
  - escena
---

Las aventueras Aerendyl, Guina, Bakel y Vhenan junto con Chandraraj y el oso de la elfa, se adentran en Visirtán tras cruzar el paso fronterizo.

<!--more-->
