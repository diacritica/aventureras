---
title: Bakel subida a unas cajas enfrentándose al Goblem y al chamán
date: 2020-03-30T02:00:00
lastmod: 2020-03-30T02:00:00
author: Guina
cover: /img/bakel_encima_cajas.jpg
categories:
  - Presentes Sangrientos
tags:
  - Bakel
  - escena
---

Bakel no puede impedir ser descubierta por el chamán que inmediatamente pronuncia la palabra de mando para despertar al goblem. Bakel busca refugio y una posición de ventaja en unas cajas apiladas en el extremo norte de la caverna.

<!--more-->
