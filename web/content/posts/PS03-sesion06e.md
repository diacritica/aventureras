---
title: Vhenan protege heroicamente a Bakel del ataque del Goblem
date: 2020-03-30T04:00:00
lastmod: 2020-03-30T04:00:00
author: Guina
cover: /img/vhenan_bakel_goblem.jpg
categories:
  - Presentes Sangrientos
tags:
  - Vhenan
  - Bakel
  - escena
---

El Goblem parece tener contra las cuerdas a Bakel, pero Vhenan, con rápidos movimientos marciales, se interpone y con su escudo protege a la tiefling de un tremendo golpe del Goblem.

<!--more-->
