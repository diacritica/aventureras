---
title: Marisha van Emris y Chandraraj debaten un posible encargo
date: 2020-02-20T08:00:00
lastmod: 2020-02-20T08:00:00
author: Guina
cover: /img/marisha_chandraraj_desayuno.jpg
categories:
  - Presentes Sangrientos
tags:
  - escena
  - PNJ
---

Durante el desayuno, Marisha van Emris y Chandraraj se ponen al día. Asisten al desayuno los hijos e hijas de Marisha mientras desde la distancia, el "séquito" del embajador escucha cómo se debate un importante encargo que puede poner fin a semanas de angustia en Alameda. Necesitan ayuda ahora que la Guardia de Alameda está muy ocupada con la inestabilidad en la frontea con Visirtán.

<!--more-->
