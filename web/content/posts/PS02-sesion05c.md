---
title: Guina, figura 3D de Hero Forge
date: 2020-02-07T02:00:00
lastmod: 2019-02-07T02:00:00
author: Guina
cover: /img/guina_3d.jpg
categories:
  - Presentes Sangrientos
tags:
  - personaje
  - Guina
---

Diseño en 3D del personaje de Guina Poleas, de Esther, empleando el software de diseño Hero Forge.

<!--more-->
