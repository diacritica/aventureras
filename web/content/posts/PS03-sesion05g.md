---
title: En la posada de El Muérdago, Alameda
date: 2020-02-20T02:00:00
lastmod: 2020-02-20T02:00:00
author: Guina
cover: /img/cena_muerdago.jpg
categories:
  - Presentes Sangrientos
tags:
  - escena
  - grupo
---

Una vez en el pueblo de Alameda, el grupo disfruta de una estupenda cena en la posada de El Muérdago, regentada por Zenobia. La mesa rebosa de buena comida y bebida tras unas duras jornadas de viaje. En un momento dato, Bakel se percata de una tensa conversación convertida en humillante presa entre un joven noble orgulloso y una extraña mujer que no tolera su insolencia.
<!--more-->
