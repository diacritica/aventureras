---
title: El chamán trabaja en el Goblem dentro de la gruta
date: 2020-03-30T01:00:00
lastmod: 2020-03-30T01:00:00
author: Guina
cover: /img/goblem.png
categories:
  - Presentes Sangrientos
tags:
  - PNJ
  - escena
---

Adentrándose en la gruta con extrema cautela, descubren al final de un pasaje, una enorme caverna en donde un chamán goblin trabaja afanosamente en su gran obra, un Gólem de aspecto goblinoide.

<!--more-->
