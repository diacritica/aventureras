---
title: El Goblem, sufriendo el halo de luna que lo envuelve, es vencido finalmente
date: 2020-03-30T06:00:00
lastmod: 2020-03-30T06:00:00
author: Guina
cover: /img/goblem_muere.jpg
categories:
  - Presentes Sangrientos
tags:
  - escena
---

El hechizo de halo de luna que invocó Guina y que abrasaba el cuerpo del Goblem junto con el hostigamiento de Vhenan y Bakel finalmente acaba con la monstruosidad.

<!--more-->
