---
title: Un extraño círculo de huesos cerca de las Cuevas Fantasmales
date: 2020-02-20T09:00:00
lastmod: 2020-02-20T09:00:00
author: Guina
cover: /img/circulo_huesos.jpg
categories:
  - Presentes Sangrientos
tags:
  - escena
---

El encargo de Marisha Van Emris que las aventureras aceptan consiste en investigar qué mal se esconde tras unos extraños círculos de huesos cerca de las Cuevas Fantasmales al norte de Alameda. La población no se atreve a ir hacia allí a recoger las famosas setas y está afectando a la economía de muchas familias.

<!--more-->
