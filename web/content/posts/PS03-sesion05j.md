---
title: Vhenan se da un baño fragante antes de dormir en el palacete de Marisha Van Emris
date: 2020-02-20T05:00:00
lastmod: 2020-02-20T05:00:00
author: Guina
cover: /img/vhenan_relax.jpg
categories:
  - Presentes Sangrientos
tags:
  - escena
  - Vhenan
---

Vhenan se abandona a un baño con sales perfumadas momentos antes de caer rendida en su cama.

<!--more-->
