---
title: Lavar y secar la ropa antes de pasar la noche en el palecete de Marisha Van Emris
date: 2020-02-20T04:00:00
lastmod: 2020-02-20T04:00:00
author: Guina
cover: /img/hacendosas_en_alameda.jpg
categories:
  - Presentes Sangrientos
tags:
  - escena
  - Guina
  - Bakel
---

Bakel y Guina atienden sus ropajes tras un viaje accidentado. Bakel se afana en lavar su ropa mientras Guina invoca al viento para provocar una brisa que seque antes su ropa limpia y húmeda.

<!--more-->
