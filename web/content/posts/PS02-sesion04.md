---
title: Cartel anunciador de la partida
date: 2019-12-016T00:00:00
lastmod: 2019-12-16T00:00:00
author: DM
cover: /img/convocatoria.png
categories:
  - Presentes Sangrientos
tags:
  - cartel
---

Cartel anunciador de la IV sesión de la aventura Presentes Sangrientos. Fotografía tomada el día de Halloween. De izquierda a derecha, Bakel (Miryam), Aerendyl (Marina), Kara (María) y Guina (Esther). Falta Vhenan (Angela)

<!--more-->
