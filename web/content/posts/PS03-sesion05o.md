---
title: Taponar con una gran roca la entrada a una gruta
date: 2020-02-20T10:00:00
lastmod: 2020-02-20T10:00:00
author: Guina
cover: /img/roca_con_x.jpg
categories:
  - Presentes Sangrientos
tags:
  - escena
  - grupo
---

Cerca del círculo de huesos que ha estado espantando a los habitantes de la Alameda, las aventureras encuentran lo que parece ser una entrada a una gruta subterránea. Gracias a la magia de Kara, reducen de peso y tamaño una enorme roca el tiempo suficiente para colocarla justo encima del agujero antes de que vuelva a su tamaño y peso original. Precavidamente, han pintado una X en la base de la roca por si quisieran poder reconocerla desde el interior de la gruta.

<!--more-->
