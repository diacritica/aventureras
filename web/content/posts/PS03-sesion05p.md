---
title: Hongos púrpura a la entrada de la gruta
date: 2020-02-20T11:00:00
lastmod: 2020-02-20T11:00:00
author: Guina
cover: /img/hongos_cueva_goblins.jpg
categories:
  - Presentes Sangrientos
tags:
  - escena
---

Las aventureras encuentran lo que parece una segunda entrada a la gruta aproximándose por la ribera oeste del Lago Liafdag. Al superar un pasaje muy estrecho, llegan a un lugar más amplio y encuentran un ambiente de tonos púrpuras y un suelo cubiertos en zonas por extraños hongos púrpuras.

<!--more-->
